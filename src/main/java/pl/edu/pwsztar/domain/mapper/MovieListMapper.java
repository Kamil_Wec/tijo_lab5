package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MovieListMapper implements ConverterMovie<List<MovieDto>,List<Movie>>{
    @Override
    public List<MovieDto> convert(List<Movie> movies) {
        List<MovieDto> moviesDto = movies
                .stream()
                .map(movie -> new MovieDto(movie.getMovieId(),  movie.getTitle(), movie.getImage(), movie.getYear()))
                .collect(Collectors.toList());

        return moviesDto;
    }
}
